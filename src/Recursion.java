/**
 * Lab 12.
 * @author ben albrecht.
 *
 */
public class Recursion {
    
    /**
     * Given base and n that are both 1 or more
     * compute recursively (no loops) the value of base to the n power
     * so powerN(3, 2) is 9 (3 squared)
     * @param base is the base of the exponent.
     * @param n is the power of the exponent.
     * @return the result of the exponent.
     */
    public static int powerN(int base, int n) { 
        if(n <= 1) {
            return base;
        }
          
        return base * powerN(base, n - 1);  
    }
    
    
    
    /**
     * We have triangle made of blocks. The topmost row has 1 block
     * the next row down has 2 blocks, the next row has 3 blocks, and so on. 
     * Compute recursively (no loops or multiplication) the total number of blocks 
     * in such a triangle with the given number of rows.
     * @param row is the number of rows in the triangle.
     * @return the number of blocks in the triangle.
     */
    public static int triangle(int row) {
        if (row == 0) {
            return 0;
          }
          
          return row + triangle(row - 1);
    }
    
    /**
     * Main method to test the two methods.
     * @param args idk what its for.
     */
    public static void main(String[] args) {
        System.out.println(powerN(3,1) == 3);
        System.out.println(powerN(3,2) == 9);
        System.out.println(powerN(3,3) == 27);
        System.out.println(triangle(0) == 0);
        System.out.println(triangle(1) == 1);
        System.out.println(triangle(2) == 3);
    }
    
}
